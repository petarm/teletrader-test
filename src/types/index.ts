export interface User {
  firstName: string;
  lastName: string;
  description: string;
  avatar: string;
}

export type CurrentUser = User | null;

export type ChannelSymbol =
  | 'tBTCUSD'
  | 'tBTCEUR'
  | 'tETHUSD'
  | 'tETHEUR'
  | 'tEOSUSD';

export interface Channel {
  id?: number;
  symbol: ChannelSymbol;
  dailyChange?: number;
  volume?: number;
  lastPrice?: number;
}

export interface ChannelInfoMessage {
  chanId: number;
  symbol: ChannelSymbol;
}

export type TickerMessage = [
  chanId: number,
  info: [
    bid: number,
    bidSize: number,
    ask: number,
    askSize: number,
    dailyChange: number,
    dailyChangeRelative: number,
    lastPrice: number,
    volume: number,
    high: number,
    low: number,
  ],
];

export type MessageResponse = ChannelInfoMessage | TickerMessage;