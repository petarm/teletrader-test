import React, { FC } from 'react';
import { Button, Container, Nav, Navbar, } from 'react-bootstrap';
import { CurrentUser, User } from '../types';
import { useHistory, useLocation } from "react-router-dom";

export interface HeaderProps {
  title: string;
  user: CurrentUser;
  setUser: React.Dispatch<React.SetStateAction<CurrentUser>>;
}

const defaultUser: User = {
  firstName: 'Petar',
  lastName: 'Markovic',
  avatar: 'https://i.picsum.photos/id/739/285/285.jpg?hmac=iRGL9xkDVQuo46ayR3SD121kvjzO6PZ6WUeV3B_CArY',
  description: 'Some quick example text to build on the card title.',
};

const Header: FC<HeaderProps> = ({ title, user, setUser }) => {
  const history = useHistory();
  const location = useLocation();

  const userLogin = (): void => {
    setUser(defaultUser);
    localStorage.setItem('currentUser', JSON.stringify(defaultUser));
  };

  return (
    <header className="Header-header">
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand>{title}</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link
              active={location.pathname === '/'}
              onClick={() => {
                history.push("/");
              }}
            >
              Home
            </Nav.Link>
            {null !== user && (
              <Nav.Link
                active={location.pathname === '/profile'}
                onClick={() => {
                  history.push("/profile");
                }}
              >
                Profile
              </Nav.Link>
            )}
          </Nav>
          {null !== user ? (
            <Nav.Link disabled>
              Welcome {user.firstName}
            </Nav.Link>
          ) : (
            <Button onClick={userLogin}>
              Login
            </Button>
          )}
        </Container>
      </Navbar>
    </header>
  );
}

export default Header;
