import React, { FC, useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from '../pages/Home';
import Profile from '../pages/Profile';
import { CurrentUser } from '../types';
import Header from './Header';

export interface AppProps {}

const App: FC<AppProps> = () => {
  const currentUser: CurrentUser = localStorage.getItem('currentUser')
    ? JSON.parse(localStorage.getItem('currentUser') as string)
    : null;

  const [user, setUser] = useState<CurrentUser>(currentUser);

  return (
    <div className="App">
      <Router>
        <Header
          title={process.env.REACT_APP_TITLE as string}
          user={user}
          setUser={setUser}
        />
        <main>
          <Switch>
            <Route path="/" exact>
              <Home apiUrl={process.env.REACT_APP_BITFINEX_DSN as string} />
            </Route>
            <Route path="/profile" exact>
              <Profile
                user={user}
                setUser={setUser}
              />
            </Route>
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;
