import React, { FC } from 'react';
import { Card, Container } from 'react-bootstrap';

export interface PageProps {}

const Page: FC<PageProps> = ({ children }) => {
  return (
    <Container className="mt-3">
      <Card className="p-3">
        {children}
      </Card>
    </Container>
  );
};

export default Page;
