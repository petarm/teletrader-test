import React, { FC } from 'react';
import { Button, Card } from 'react-bootstrap';
import { CurrentUser } from '../types';
import Page from './Page';
import { User } from './../types';

export interface ProfileProps {
  user: CurrentUser;
  setUser: React.Dispatch<React.SetStateAction<CurrentUser>>;
}

const changeAvatar = (
  {
    firstName,
    lastName,
    description,
  }: User,
  setUser: React.Dispatch<React.SetStateAction<CurrentUser>>
): void => {
  console.log('dasdsadasdsa');
  setUser({
    firstName,
    lastName,
    description,
    avatar: `https://picsum.photos/285/285?hack${Math.random()}`,
  });
};

const Profile: FC<ProfileProps> = ({ user, setUser }) => {
  if (null === user) {
    return (
      <Page>
        You are not logged in.
      </Page>
    );
  }

  return (
    <Page>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={user.avatar} />
        <Card.Body>
          <Card.Title>{user.firstName} {user.lastName}</Card.Title>
          <Card.Text>{user.description}</Card.Text>
          <Button variant="primary" onClick={() => {
            changeAvatar(user as User, setUser);
          }}>Toggle avatar</Button>
        </Card.Body>
      </Card>
    </Page>
  );
};

export default Profile;
