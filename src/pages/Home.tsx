import React, { FC, useState, useEffect, useRef } from 'react';
import { Table } from 'react-bootstrap';
import Page from './Page';
import {
  Channel,
  ChannelSymbol,
  MessageResponse,
  ChannelInfoMessage,
  TickerMessage
} from './../types';

export interface HomeProps {
  apiUrl: string;
}

const Home: FC<HomeProps> = ({
  apiUrl,
}) => {
  const symbols: ChannelSymbol[] = [
    'tBTCUSD',
    'tBTCEUR',
    'tETHUSD',
    'tETHEUR',
    'tEOSUSD',
  ];

  const [channels, setChannels] = useState<Channel[]>(
    symbols.map((symbol): Channel => ({
      symbol,
      id: undefined,
      dailyChange: undefined,
      volume: undefined,
      lastPrice: undefined,
    }))
  );


  const ws = useRef<WebSocket | null>(null);
  const [connected, setConnected] = useState<boolean>(false)

  useEffect(() => {
    if (connected) {
      return;
    }

    ws.current = new WebSocket(apiUrl, "protocolOne");
    ws.current.onopen = (): void => {
      channels.forEach(({ symbol }): void => {
        (ws.current as WebSocket).send(JSON.stringify({
          event: 'subscribe',
          channel: 'ticker',
          symbol,
        }));
      });
    };

    setConnected(true);

    return () => {
      if (!connected) {
        return;
      }

      (ws.current as WebSocket).close();
    };
  }, [connected]);

  useEffect(() => {
    if (!ws.current) {
      return;
    }

    let newChannels: Channel[] = channels;

    ws.current.onmessage = (event): void => {
      const data: MessageResponse = JSON.parse(event.data);

      if (data.hasOwnProperty('chanId')) {
        const channelId = (data as ChannelInfoMessage).chanId;

        newChannels = newChannels.map((channel): Channel => {
          if (channel.symbol === (data as ChannelInfoMessage).symbol) {
            return {
              ...channel,
              id: channelId,
            };
          }

          return {
            ...channel,
          };
        });
      }

      if (!Array.isArray((data as TickerMessage)[1])) {
        return;
      }

      const channelId = (data as TickerMessage)[0];
      const [
        _bid,
        _bidSize,
        _ask,
        _askSize,
        _dailyChange,
        dailyChangeRelative,
        lastPrice,
        volume,
        _high,
        _low,
      ] = (data as TickerMessage)[1];

      newChannels = newChannels.map((channel): Channel => {
        if (channel.id === channelId) {
          return {
            ...channel,
            dailyChange: Math.round(dailyChangeRelative * 10000) / 100,
            volume: Math.round(volume * 100) / 100,
            lastPrice: Math.round(lastPrice * 100) / 100,
          };
        }

        return {
          ...channel,
        };
      });

      setChannels(newChannels);
    };
  });

  return (
    <Page>
       <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Symbol</th>
            <th>Daily change</th>
            <th>Volume</th>
            <th>Last Price</th>
          </tr>
        </thead>
        <tbody>
          {channels.map(({
            symbol,
            dailyChange,
            volume,
            lastPrice,
          }, i) => (
            <tr key={symbol}>
              <td>{i+1}</td>
              <td>{symbol.substr(1)}</td>
              <td>{dailyChange || 'NaN'}%</td>
              <td>{volume}</td>
              <td>{lastPrice}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Page>
  );
};

export default Home;
